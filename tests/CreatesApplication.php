<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;

trait CreatesApplication
{
    /**
     * Creates the application.
     */
    public function createApplication(): Application
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    function automationIntiConstanct(){
          return [
            // 'funnelBaseUrl'=>"http://orders--test.porterandcompanyresearch.com/",
            // "funnelBaseUrl"=>"https://porterandcompanyresearch.co/",
            "funnelBaseUrl"=>"https://stage:26K6Vsn8adgqgH3zcduGjzvLOyuz9GxLXGXqHTDSWTE=@staging.porterandcompanyresearch.co/",
            'promntUser'=>"stage",
            'promntPass'=>" 26K6Vsn8adgqgH3zcduGjzvLOyuz9GxLXGXqHTDSWTE=",
        ];
    }
}
