<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Tests\Browser\Pages\HomePage;

use Tests\Browser\Components\DatePicker;

// php artisan dusk --log-junit=public/output/dusk-report.xml  
// php artisan dusk --filter DownSellTest::testFunnelOrders --log-junit=public/output/dusk-report-download.xml

class DownSellTest extends DuskTestCase
{
    // use DatabaseMigrations, DatabaseTruncation; 
    // protected $tablesToTruncate = ['users'];
    private $funnels=[];
    /**
     * A Dusk test example.
     */
 
    //  public function testPromtLogin(): void
    // {
    //     $this->config=$this->automationIntiConstanct(); 
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit($this->config['funnelBaseUrl'])->typeSlowly($this->config['promntUser'])
    //         ->typeSlowly($this->config['promntPass'])->press('Sign in');
    //     });
    // }



    // public function testLogin(): void
    // {
    //     $this->config=$this->automationIntiConstanct(); 
        
        
  
    //     // $user = User::factory()->create([
    //     //     'email' => 'taylor@laravel.com',
    //     // ]);
 
    //     // $this->browse(function (Browser $browser) use ($user) {
    //     //     $browser->visit('/login')
    //     //             ->type('email', $user->email)
    //     //             ->type('password', 'password')
    //     //             ->press('Login')
    //     //             ->assertPathIs('/home');
    //     // });


    //     $this->browse(function (Browser $browser) {
    //         $browser->visit($this->config['funnelBaseUrl'].'cusw/?source=test');
    //         $browser->scrollIntoView('.legal-footer-container-1');
    //         $browser->script("$('.elBTN').show();");
    //         $browser->pause(3000);
    //         $browser->press('.elButtonMain');
    //         $browser->pause(5000);
    //         $browser->pause(2000)->screenshot('ReadPage');
    //         $browser->scrollIntoView('.legal-footer-container-1');
    //         $browser->pause(1000);
    //         $browser->press('.elButtonMain');
    //         $browser->pause(2000);
    //         $browser->scrollIntoView('.payment-form');
    //         $browser->typeSlowly('fn','FN_'.date('Ymd'));
    //         $browser->typeSlowly('ln','LN_'.date('Hi'));
    //         $browser->typeSlowly('email','hitesh.quiz'.date('Ymdhi').'@netesenz.in');
    //         $browser->typeSlowly('phone','7737595951');
    //         $browser->typeSlowly('street','Nemi nagar, Ajmer road');
    //         $browser->typeSlowly('city','Baltimore');
    //         $browser->typeSlowly('state','MD');
    //         $browser->typeSlowly('zip','20201');
    //         $browser->typeSlowly('ccno','4242424242424242');
    //         $browser->typeSlowly('cccode','111');
    //         $browser->pause(3000); 
    //         // $browser->waitUntilEnabled(3000); 
    //         $browser->select('ccyr','2025');
    //         $browser->select('ccmo',date('m'));     
    //         $browser->press('Place Order');  
            
    //         $browser->pause(40000); 
    //         $browser->scrollIntoView('.legal-footer-container-1');
    //         $browser->pause(5000); 
    //         $browser->clickAtXPath('(//*[@id="subbtn"])[4]');
    //     });
    // }

     
    public function testFunnelOrders(): void
    {
        $this->config=$this->automationIntiConstanct();   
        $this->funnels=['aisk','aiskb','gdi','lgtab','tbdu','taiid'];
        $this->browse(function (Browser $browser) {
            foreach($this->funnels as $funnelName){
            $browser->visit($this->config['funnelBaseUrl'].$funnelName.'/order/?source=test&pageReview=true');
            $browser->pause(3000);
            $browser->scrollIntoView('.payment-form');
            $browser->typeSlowly('fn','FN_'.date('Ymd'));
            $browser->typeSlowly('ln','LN_'.date('Hi'));
            $browser->typeSlowly('email','hitesh.quiz'.date('Ymdhi').'@netesenz.in');
            $browser->typeSlowly('phone','7737595951');
            $browser->typeSlowly('street','Nemi nagar, Ajmer road');
            $browser->typeSlowly('city','Baltimore');
            $browser->typeSlowly('state','MD');
            $browser->typeSlowly('zip','20201');
            $browser->typeSlowly('ccno','4242424242424242');
            $browser->typeSlowly('cccode','111');
            $browser->pause(3000); 
            // $browser->waitUntilEnabled(3000); 
            $browser->select('ccyr','2025');
            $browser->select('ccmo',date('m')); 
            $browser->scrollIntoView('.mainsellOrderBtn');   
            $browser->pause(1000); 
            $browser->clickAtXPath('(//*[@id="subbtn"])[1]')->screenshot($funnelName."-mainsell");

            $browser->pause(10000); 
            $browser->scrollIntoView('#payment-form');
            $browser->pause(5000)->screenshot($funnelName."-upsell"); 
            $browser->clickAtXPath('(//*[@id="subbtn"])[1]');
            $browser->pause(1000); 
            $browser->scrollIntoView('.product_information');
            $browser->pause(10000)->screenshot($funnelName."-thankyou");
            }
        });
    }
    

    // public function testFunnelOrderThanskyou(): void
    // {
    //     $this->config=$this->automationIntiConstanct();   

    //     $this->browse(function (Browser $browser) {
    //         $funnelName="psibc";
    //         $browser->visit($this->config['funnelBaseUrl'].$funnelName.'/order/?source=test&pageReview=true');
    //         $browser->pause(3000);
    //         $browser->scrollIntoView('.payment-form');
    //         $browser->typeSlowly('fn','FN_'.date('Ymd'));
    //         $browser->typeSlowly('ln','LN_'.date('Hi'));
    //         $browser->typeSlowly('email','hitesh.quiz'.date('Ymdhi').'@netesenz.in');
    //         $browser->typeSlowly('phone','7737595951');
    //         $browser->typeSlowly('street','Nemi nagar, Ajmer road');
    //         $browser->typeSlowly('city','Baltimore');
    //         $browser->typeSlowly('state','MD');
    //         $browser->typeSlowly('zip','20201');
    //         $browser->typeSlowly('ccno','4242424242424242');
    //         $browser->typeSlowly('cccode','111');
    //         $browser->pause(3000); 
    //         // $browser->waitUntilEnabled(3000); 
    //         $browser->select('ccyr','2025');
    //         $browser->select('ccmo',date('m'));    
    //         $browser->scrollIntoView('.mainsellOrderBtn');   
    //         $browser->pause(1000); 
    //         $browser->clickAtXPath('(//*[@id="subbtn"])[1]')->screenshot($funnelName."-mainsell");
    //          $browser->pause(1000); 
    //         $browser->scrollIntoView('.product_information');
    //         $browser->pause(10000)->screenshot($funnelName."-thankyou");
    //     });
    // }


    // public function testXpath(): void
    // {
    //     $this->config=$this->automationIntiConstanct(); 
        
         
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('https://stage:26K6Vsn8adgqgH3zcduGjzvLOyuz9GxLXGXqHTDSWTE=@staging.porterandcompanyresearch.co/psfp/order/?source=test&pageReview=true');
    //         $browser->pause(40000); 
    //         // $browser->scrollIntoView('.progressbar_nocorners');
    //         $browser->scrollIntoView('.legal-footer-container-1');
    //         $browser->pause(5000); 
    //         $browser->clickAtXPath('(//*[@id="subbtn"])[4]');
    //         $browser->pause(10000); 
    //         // $browser->press('xpath=/html/body/div[14]/div/div[2]/div/div/div[22]/div/div/form/button[1]');
    //         // $browser->press('xpath=(//a[@class="elButtonMain"])[1]');
            
    //     });
    // }

   

    // public function test_basic_example(): void
    // {
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('/')
    //                 ->within(new DatePicker, function (Browser $browser) {
    //                     $browser->selectDate(2019, 1, 30);
    //                 })
    //                 ->assertSee('January');
    //     });
    // }
}
