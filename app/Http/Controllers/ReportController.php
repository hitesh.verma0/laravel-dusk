<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Routing\Controller as BaseController;

class ReportController extends Controller
{
  function report(){ 
    // $xmlString = 'output/dusk-report.xml'; 
    
    $xmlString = 'output/dusk-mainsell-report.xml'; 

    // $xmlString = file_get_contents(storage_path() . "/json/ashan.xml"));
    
    // $xmlString = file_get_contents($xmlString);
    $xmlObject = simplexml_load_file($xmlString);
            
    $json = json_encode($xmlObject);
    $array = json_decode($json, true); 
      //  foreach ($array as $index => $value) {
      //       if(!empty($value['@attributes'])){
      //           foreach($value['@attributes'] as $loop =>$attribute){
                    
      //                echo "Testsuite $loop: " . $attribute . "<br/>";
      //           }
      //       }
      //       // Access other attributes and elements as needed
      //   } 

      // dd($array['testsuite']['testsuite']['testsuite']['testcase']);
    if($array['testsuite']['testsuite']['testsuite']){
      $count =1;
        foreach ($array['testsuite']['testsuite']['testsuite']['testcase'] as $index => $testCase) {
          // dd($testCase['@attributes']);
          if(!empty($testCase['@attributes'])){


            // if($count==2){
            //   dd($testCase);
            // }

            echo "$count <br/> ";
              foreach($testCase['@attributes'] as $loop =>$attribute){
                // dd($loop,$attribute);
                  
                   echo "Testsuite $loop: " . $attribute . "<br/>"; 

              } 
              if(!empty($testCase['error'])){
                echo "Status : Failed <br/>"; 
                echo "Failure Reason: " . $testCase['error'] . "<br/>"; 
               }else{
                echo "Status : Pass"; 
               }
              $count++;
          }
          
          // Access other attributes and elements as needed
      } 
    }

      
  }   
  
}
